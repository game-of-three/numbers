FROM dockerproxy-iva.si.francetelecom.fr/openjdk:11-jre

LABEL Name="Numbers"
LABEL Description="Backend for Numbers service"
LABEL Maintainer="mahmoudragab726@gmail.com"
LABEL Url="https://gitlab.com/game-of-three/numbers"
COPY ./target/numbers*.jar numbers.jar

EXPOSE 8080

HEALTHCHECK --start-period=60s \
  CMD curl -f http://localhost:8080/actuator/health || exit 1

USER 1001
ENTRYPOINT ["java","-jar","/numbers.jar"]

package com.takeaway.gameofthree.numbers.controller;

import com.takeaway.gameofthree.numbers.common.error.ApiError;
import com.takeaway.gameofthree.numbers.component.number.dto.NumberWrapper;
import io.swagger.annotations.*;

import javax.validation.constraints.Min;
import java.net.HttpURLConnection;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/27/2020 by OLE
 */
@Api(value = "/numbers")
public interface NumbersApi {

    @ApiOperation(value = "Get the initial number to start the game", response = NumberWrapper.class)
    @ApiResponses({@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "The initial start number has been returned", response = NumberWrapper.class),
            @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "Some parameters are missing or invalid", response = ApiError.class)})
    NumberWrapper getInitialStartNumber();

    @ApiOperation(value = "Get the next which is dividable by 3 after adding one of {-1, 0, 1}", response = NumberWrapper.class)
    @ApiResponses({@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "The next number has been  calculated and returned", response = NumberWrapper.class),
            @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "Some parameters are missing or invalid", response = ApiError.class)})
    NumberWrapper getNextNumber(
            @ApiParam(value = "The base number to get it's next number after adding onr of this set {-1, 0, 1}", required = true) @Min(2) int number);

}

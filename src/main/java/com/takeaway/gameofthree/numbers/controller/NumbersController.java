package com.takeaway.gameofthree.numbers.controller;

import com.takeaway.gameofthree.numbers.component.number.NumbersService;
import com.takeaway.gameofthree.numbers.component.number.dto.NumberWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;


/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/27/2020 by OLE
 */
@RestController
@RequestMapping("/numbers")
@Validated
public class NumbersController implements NumbersApi {

    private NumbersService numbersService;

    @Autowired
    public NumbersController(NumbersService numbersService) {
        this.numbersService = numbersService;
    }

    @Override
    @GetMapping("/init")
    @ResponseStatus(HttpStatus.OK)
    public NumberWrapper getInitialStartNumber() {
        return numbersService.getInitialStartNumber();
    }

    @Override
    @GetMapping("/next")
    @ResponseStatus(HttpStatus.OK)
    public NumberWrapper getNextNumber(@RequestParam @Min(2) int number) {
        return numbersService.getNextNumber(number);
    }
}

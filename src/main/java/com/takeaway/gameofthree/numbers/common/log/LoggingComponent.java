package com.takeaway.gameofthree.numbers.common.log;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/27/2020 by OLE
 */


import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.lang.annotation.Annotation;

@Aspect
@Component
@Slf4j
public class LoggingComponent {
    private static final String POINTCUT = "execution(* com.takeaway.gameofthree.numbers.component.*.*.*(..))";

    /**
     * Input logging
     *
     * @param joinPoint
     */
    @Before(POINTCUT)
    public void logBefore(JoinPoint joinPoint) {
        log.info("> {}{}", joinPoint.getSignature().getName(), argumentsToString(joinPoint));
    }

    /**
     * Output logging
     *
     * @param joinPoint
     * @param result
     */
    @AfterReturning(pointcut = POINTCUT, returning = "result")
    public void logAfterResult(JoinPoint joinPoint, Object result) {
        String entityString = "<empty>";
        if (result != null) {
            entityString = result.toString();
        }
        log.info("< {}{} = {}", joinPoint.getSignature().getName(), argumentsToString(joinPoint), entityString);
    }

    /**
     * Exception logging
     *
     * @param joinPoint
     * @param e
     */
    @AfterThrowing(pointcut = POINTCUT, throwing = "e")
    public void logAfterException(JoinPoint joinPoint, Exception e) {
        log.error("< {}{} = {}", joinPoint.getSignature().getName(), argumentsToString(joinPoint), e.toString());
    }

    /**
     * Build the string listing the joinPoint arguments
     *
     * @param joinPoint
     * @return
     */
    private String argumentsToString(JoinPoint joinPoint) {
        StringBuilder argsString = new StringBuilder("[");

        if (joinPoint.getSignature() instanceof MethodSignature) {
            Object[] args = joinPoint.getArgs();
            Annotation[][] annotations = ((MethodSignature) joinPoint.getSignature()).getMethod().getParameterAnnotations();

            if (null != args) {
                for (int i = 0; i < args.length; i++) {
                    argsString.append(argumentToString(annotations[i], args[i]));

                    if (i != args.length - 1) {
                        argsString.append(", ");
                    }
                }
            }
        }
        argsString.append("]");

        return argsString.toString();
    }

    /**
     * Build the string for an argument
     * Parameters names are deduced from annotations
     *
     * @param annotations
     * @param argValue
     * @return
     */
    private String argumentToString(Annotation[] annotations, Object argValue) {
        StringBuilder argString = new StringBuilder();
        String argName = null;
        for (Annotation annotation : annotations) {
            if (annotation instanceof PathVariable) {
                argName = ((PathVariable) annotation).value();

            } else if (annotation instanceof RequestParam) {
                argName = ((RequestParam) annotation).value();

            } else if (annotation instanceof RequestHeader) {
                argName = ((RequestHeader) annotation).name();
            }
        }
        if (null != argName) {
            argString.append(argName).append("@");
        }
        argString.append(argValue);

        return argString.toString();
    }
}

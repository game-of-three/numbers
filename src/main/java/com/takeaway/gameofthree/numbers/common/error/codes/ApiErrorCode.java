package com.takeaway.gameofthree.numbers.common.error.codes;

import com.takeaway.gameofthree.numbers.common.error.ApiError;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/27/2020 by OLE
 */
public enum ApiErrorCode {

    INVALID_QUERY_PARAM_VALUE(28, HttpStatus.BAD_REQUEST, "Invalid query-string parameter value"),

    MISSING_QUERY_PARAM(27, HttpStatus.BAD_REQUEST, "Missing query-string parameter");

    @Getter
    private final int code;

    @Getter
    private final HttpStatus httpStatus;

    @Getter
    private final String message;

    ApiErrorCode(int code, HttpStatus httpStatus, String message) {
        this.code = code;
        this.httpStatus = httpStatus;
        this.message = message;
    }


    public ApiError toResponseEntity(String details) {
        return new ApiError(code, name(), message + ": " + details);
    }

    public ApiError toResponseEntity() {
        return new ApiError(code, name(), message);
    }
}

package com.takeaway.gameofthree.numbers.component.number;

import com.takeaway.gameofthree.numbers.component.number.dto.NumberWrapper;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/27/2020 by OLE
 */
public interface NumbersService {
    NumberWrapper getInitialStartNumber();

    NumberWrapper getNextNumber(int number);
}

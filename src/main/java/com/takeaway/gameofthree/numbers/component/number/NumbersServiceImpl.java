package com.takeaway.gameofthree.numbers.component.number;

import com.takeaway.gameofthree.numbers.component.number.dto.NumberWrapper;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/27/2020 by OLE
 */
@Service
@Slf4j
public class NumbersServiceImpl implements NumbersService {

    @Value("${numbers.range.min}")
    @Setter(AccessLevel.PACKAGE)
    private Integer min;
    @Value("${numbers.range.max}")
    @Setter(AccessLevel.PACKAGE)
    private Integer max;
    private Random random = new Random();

    @Override
    public NumberWrapper getInitialStartNumber() {
        int randomNumber = random.nextInt((max - min) + 1) + min;
        return NumberWrapper.builder()
                .number(randomNumber)
                .build();
    }

    @Override
    public NumberWrapper getNextNumber(int number) {
        return getNumberDividedByThreeAndTheAddedValue(number);
    }


    NumberWrapper getNumberDividedByThreeAndTheAddedValue(int number) {
        int result;
        int addedValue;

        if ((number + 1) % 3 == 0) {
            result = (number + 1) / 3;
            addedValue = 1;
        } else if ((number - 1) % 3 == 0) {
            result = (number - 1) / 3;
            addedValue = -1;
        } else {
            result = number / 3;
            addedValue = 0;
        }
        log.info("The added value is -> {} and the result of division is -> {} for the number -> {}", addedValue, result, number);

        return NumberWrapper.builder()
                .number(result)
                .addedValue(addedValue)
                .build();
    }


}

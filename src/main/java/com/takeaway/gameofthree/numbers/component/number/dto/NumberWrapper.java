package com.takeaway.gameofthree.numbers.component.number.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/27/2020 by OLE
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NumberWrapper {
    private Integer number;
    private Integer addedValue;

}

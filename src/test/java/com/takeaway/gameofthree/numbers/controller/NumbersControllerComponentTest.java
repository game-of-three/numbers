package com.takeaway.gameofthree.numbers.controller;

import com.takeaway.gameofthree.numbers.component.number.dto.NumberWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/27/2020 by OLE
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class NumbersControllerComponentTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private static final String BASE_URI = "/numbers";

    @Value("${numbers.range.min}")
    private Integer min;
    @Value("${numbers.range.max}")
    private Integer max;


    @Test
    void whenGetInitialStartNumber_ThenSuccessWithStatus_200() {
        ResponseEntity<NumberWrapper> response = restTemplate.getForEntity(BASE_URI + "/init", NumberWrapper.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getNumber()).isBetween(min, max);
    }

    @Test
    void whenGetNextNumber_ThenSuccessWithStatus_200() {
        int number = 11;
        int addedValue = 1;
        ResponseEntity<NumberWrapper> response = restTemplate.getForEntity(BASE_URI + "/next?number=" + number, NumberWrapper.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getNumber()).isEqualTo((number + addedValue) / 3);
    }
}

package com.takeaway.gameofthree.numbers.controller;

import com.takeaway.gameofthree.numbers.common.error.codes.ApiErrorCode;
import com.takeaway.gameofthree.numbers.component.number.NumbersService;
import com.takeaway.gameofthree.numbers.component.number.dto.NumberWrapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/27/2020 by OLE
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = NumbersController.class)
class NumbersControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private NumbersService numbersService;

    private static final String BASE_URL = "/numbers";


    @Test
    void whenGetInitialRandomNumber_ThenSuccessWith_Status200() throws Exception {

        int number = 150;
        NumberWrapper response = NumberWrapper.builder()
                .number(number).build();
        given(numbersService.getInitialStartNumber()).willReturn(response);
        mockMvc
                .perform(MockMvcRequestBuilders.get(BASE_URL + "/init"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.number", is(number)));
    }

    @Test
    void whenGetNextNumberOfNumberGreaterThan_one_ThenSuccessWith_Status200() throws Exception {
        int number = 40;
        int addedValue = 0;
        NumberWrapper response = NumberWrapper.builder()
                .number(number)
                .addedValue(addedValue).build();
        given(numbersService.getNextNumber(120)).willReturn(response);
        mockMvc
                .perform(MockMvcRequestBuilders.get(BASE_URL + "/next")
                        .queryParam("number", "120"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.number", is(number)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.addedValue", is(addedValue)));
    }

    @Test
    void whenGetNextNumberOfNumberLessThan_Two_ThenFailWith_Status400() throws Exception {

        mockMvc
                .perform(MockMvcRequestBuilders.get(BASE_URL + "/next")
                        .queryParam("number", "1"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code", is(ApiErrorCode.INVALID_QUERY_PARAM_VALUE.getCode())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message", is(ApiErrorCode.INVALID_QUERY_PARAM_VALUE.name())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").isString());
    }

    @Test
    void whenGetNextNumber_WithoutSending_number_QueryParam_ThenFailWith_Status400() throws Exception {

        mockMvc
                .perform(MockMvcRequestBuilders.get(BASE_URL + "/next"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code", is(ApiErrorCode.MISSING_QUERY_PARAM.getCode())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message", is(ApiErrorCode.MISSING_QUERY_PARAM.name())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").isString());
    }

    @Test
    void whenGetNextNumber_WithInvalidBindingTypeAsNameInsteadOfNumeric_QueryParam_ThenFailWith_Status400() throws Exception {

        mockMvc
                .perform(MockMvcRequestBuilders.get(BASE_URL + "/next")
                        .queryParam("number", "string"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code", is(ApiErrorCode.INVALID_QUERY_PARAM_VALUE.getCode())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message", is(ApiErrorCode.INVALID_QUERY_PARAM_VALUE.name())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").isString());
    }
}

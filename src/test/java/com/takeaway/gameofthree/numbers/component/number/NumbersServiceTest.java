package com.takeaway.gameofthree.numbers.component.number;

import com.takeaway.gameofthree.numbers.component.number.dto.NumberWrapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @Author Mahmoud Sakr - mahmoud.sakr.ext@orange.com
 * @Created: @ 11/27/2020 by OLE
 */
@ExtendWith(MockitoExtension.class)
class NumbersServiceTest {
    @InjectMocks
    private NumbersServiceImpl numbersService;


    @Test
    void whenGetInitialStartNumberWithEqual_MinAndMaxValues_ThenReturnANumberWithinTheRangeSuccessfully() {
        int min = 2;
        int max = 2;
        numbersService.setMin(min);
        numbersService.setMax(max);
        NumberWrapper result = numbersService.getInitialStartNumber();
        assertThat(result.getNumber()).isEqualTo(2);
    }

    @Test
    void whenGetInitialStartNumberWithDifferent_MinAndMaxValues_ThenReturnANumberWithinTheRangeSuccessfully() {
        int min = 5;
        int max = 10;
        numbersService.setMin(min);
        numbersService.setMax(max);
        NumberWrapper result = numbersService.getInitialStartNumber();
        assertThat(result.getNumber()).isBetween(min, max);
    }

    @Test
    void whenGetTheNextNumberDividedByThreeAndTheAddedValue_ForANumberWhichAdd_MinusOne_ToTheNumberSuccessfully(){
        int number = 7;
        NumberWrapper result = numbersService.getNumberDividedByThreeAndTheAddedValue(number);
        assertThat(result.getNumber()).isEqualTo(2);
        assertThat(result.getAddedValue()).isEqualTo(-1);

    }

    @Test
    void whenGetTheNextNumberDividedByThreeAndTheAddedValue_ForANumberWhichAdd_One_ToTheNumberSuccessfully(){
        int number = 8;
        NumberWrapper result = numbersService.getNumberDividedByThreeAndTheAddedValue(number);
        assertThat(result.getNumber()).isEqualTo(3);
        assertThat(result.getAddedValue()).isEqualTo(1);

    }

    @Test
    void whenGetTheNextNumberDividedByThreeAndTheAddedValue_ForANumberWhichAdd_Zero_ToTheNumberSuccessfully(){
        int number = 12;
        NumberWrapper result = numbersService.getNumberDividedByThreeAndTheAddedValue(number);
        assertThat(result.getNumber()).isEqualTo(4);
        assertThat(result.getAddedValue()).isZero();
    }

    @Test
    void whenGetNextNumberSuccessfully(){
        int number = 110;
        NumberWrapper result = numbersService.getNextNumber(number);
        assertThat(result.getNumber()).isEqualTo((number+1)/3);
        assertThat(result.getAddedValue()).isEqualTo(1);
    }

}
